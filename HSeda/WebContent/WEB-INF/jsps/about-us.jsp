<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<jsp:include page="common/head.jsp" />

<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
<link rel="shortcut icon" href="images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="images/ico/apple-touch-icon-57-precomposed.png">
</head>
<!--/head-->

<body>

	<!-- header -->
	<jsp:include page="common/header.jsp" />
	<!--/header-->

	<section id="about-us">
	<div class="container">
		<div class="center wow fadeInDown">
			<h2>Seda & Perutkova, s.r.o.</h2>
			<p class="lead">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
				eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut
				enim ad minim veniam
			</p>
		</div>

		<!-- about us slider -->
		<div id="about-slider">
			<div id="carousel-slider" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators visible-xs">
					<li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-slider" data-slide-to="1"></li>
					<li data-target="#carousel-slider" data-slide-to="2"></li>
				</ol>

				<div class="carousel-inner">
					<div class="item active">
						<img src="images/slider_one.jpg" class="img-responsive" alt="">
					</div>
					<div class="item">
						<img src="images/slider_one.jpg" class="img-responsive" alt="">
					</div>
					<div class="item">
						<img src="images/slider_one.jpg" class="img-responsive" alt="">
					</div>
				</div>

				<a class="left carousel-control hidden-xs" href="#carousel-slider"
					data-slide="prev"> <i class="fa fa-angle-left"></i>
				</a> <a class=" right carousel-control hidden-xs"
					href="#carousel-slider" data-slide="next"> <i
					class="fa fa-angle-right"></i>
				</a>
			</div>
			<!--/#carousel-slider-->
		</div>
		<!--/#about-slider-->


		<!-- Our Skill -->
		<div class="skill-wrap clearfix">

			<div class="center wow fadeInDown">
				<h2>Naše dovednosti</h2>
				<p class="lead">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
					eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut
					enim ad minim veniam
				</p>
			</div>

			<div class="row">

				<div class="col-sm-3">
					<div class="sinlge-skill wow fadeInDown" data-wow-duration="1000ms"
						data-wow-delay="300ms">
						<div class="joomla-skill">
							<p>
								<em>85%</em>
							</p>
							<p>IT</p>
						</div>
					</div>
				</div>

				<div class="col-sm-3">
					<div class="sinlge-skill wow fadeInDown" data-wow-duration="1000ms"
						data-wow-delay="600ms">
						<div class="html-skill">
							<p>
								<em>95%</em>
							</p>
							<p>Právní poradenství</p>
						</div>
					</div>
				</div>

				<div class="col-sm-3">
					<div class="sinlge-skill wow fadeInDown" data-wow-duration="1000ms"
						data-wow-delay="900ms">
						<div class="css-skill">
							<p>
								<em>80%</em>
							</p>
							<p>Daně a účetnictví</p>
						</div>
					</div>
				</div>

				<div class="col-sm-3">
					<div class="sinlge-skill wow fadeInDown" data-wow-duration="1000ms"
						data-wow-delay="1200ms">
						<div class="wp-skill">
							<p>
								<em>90%</em>
							</p>
							<p>Administrativní služby</p>
						</div>
					</div>
				</div>

			</div>

		</div>
		<!--/.our-skill-->


		<!-- our-team -->
		<div class="team">
			<div class="center wow fadeInDown">
				<h2>Členové týmu</h2>
				<p class="lead">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
					eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut
					enim ad minim veniam
				</p>
			</div>

			<div class="row clearfix">
				<div class="col-md-4 col-sm-6">
					<div class="single-profile-top wow fadeInDown"
						data-wow-duration="1000ms" data-wow-delay="300ms">
						<div class="media">
							<div class="pull-left">
								<a href="#"><img class="media-object" src="images/man1.jpg"
									alt=""></a>
							</div>
							<div class="media-body">
								<h4>JUDr. Jindřiška Šedová, c.s.c.</h4>
								<h5>Zakladatelka a CEO</h5>
								<ul class="tag clearfix">
									<li class="btn"><a href="#">Obchodní Právo</a></li>
									<li class="btn"><a href="#">Poradenství</a></li>
									<li class="btn"><a href="#">Ekonomie</a></li>
								</ul>

								<ul class="social_icons">
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								</ul>
							</div>
						</div>
						<!--/.media -->
						<p>There are many variations of passages of Lorem Ipsum
							available, but the majority have suffered alteration in some
							form, by injected humour, or randomised words which don't look
							even slightly believable.</p>
					</div>
				</div>
				<!--/.col-lg-4 -->


				<div class="col-md-4 col-sm-6 col-md-offset-2">
					<div class="single-profile-top wow fadeInDown"
						data-wow-duration="1000ms" data-wow-delay="300ms">
						<div class="media">
							<div class="pull-left">
								<a href="#"><img class="media-object" src="${pageContext.request.contextPath}/static/images/zdenkaperutkova.jpg"
									alt=""></a>
							</div>
							<div class="media-body">
								<h4>Ing. Zdeňka Perutková</h4>
								<h5>Zakladatelka a CEO</h5>
								<ul class="tag clearfix">
									<li class="btn"><a href="#">Organizace</a></li>
									<li class="btn"><a href="#">Vedení</a></li>
									<li class="btn"><a href="#">Překlady</a></li>
								</ul>
								<ul class="social_icons">
									<li><a href="https://www.facebook.com/zdenka.perutkova.5?fref=ts"><i class="fa fa-facebook"></i></a></li>
									<li><a href="https://www.linkedin.com/in/zde%C5%88ka-perutkov%C3%A1-6969017a"><i class="fa fa-twitter"></i></a></li>
									<li><a href="https://plus.google.com/112831447210098799328"><i class="fa fa-google-plus"></i></a></li>
								</ul>
							</div>
						</div>
						<!--/.media -->
						<p>There are many variations of passages of Lorem Ipsum
							available, but the majority have suffered alteration in some
							form, by injected humour, or randomised words which don't look
							even slightly believable.</p>
					</div>
				</div>
				<!--/.col-lg-4 -->
			</div>
			<!--/.row -->
			<div class="row team-bar">
				<div class="first-one-arrow hidden-xs">
					<hr>
				</div>
				<div class="first-arrow hidden-xs">
					<hr>
					<i class="fa fa-angle-up"></i>
				</div>
				<div class="second-arrow hidden-xs">
					<hr>
					<i class="fa fa-angle-down"></i>
				</div>
				<div class="third-arrow hidden-xs">
					<hr>
					<i class="fa fa-angle-up"></i>
				</div>
				<div class="fourth-arrow hidden-xs">
					<hr>
					<i class="fa fa-angle-down"></i>
				</div>
			</div>
			<!--skill_border-->

			<div class="row clearfix">
				<div class="col-md-4 col-sm-6 col-md-offset-2">
					<div class="single-profile-bottom wow fadeInUp"
						data-wow-duration="1000ms" data-wow-delay="600ms">
						<div class="media">
							<div class="pull-left">
								<a href="#"><img class="media-object" src="${pageContext.request.contextPath}/static/images/pavelseda.jpg"
									alt=""></a>
							</div>

							<div class="media-body">
								<h4>Bc. Pavel Šeda</h4>
								<h5>Člen týmu</h5>
								<ul class="tag clearfix">
									<li class="btn"><a href="#">IT</a></li>
									<li class="btn"><a href="#">Java</a></li>
									<li class="btn"><a href="#">DB</a></li>
								</ul>
								<ul class="social_icons">
									<li><a href="https://www.facebook.com/pavel.seda.7"><i class="fa fa-facebook"></i></a></li>
									<li><a href="https://www.linkedin.com/in/pavel-%C5%A1eda-b5b96184"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								</ul>
							</div>
						</div>
						<!--/.media -->
						<p>There are many variations of passages of Lorem Ipsum
							available, but the majority have suffered alteration in some
							form, by injected humour, or randomised words which don't look
							even slightly believable.</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-md-offset-2">
					<div class="single-profile-bottom wow fadeInUp"
						data-wow-duration="1000ms" data-wow-delay="600ms">
						<div class="media">
							<div class="pull-left">
								<a href="#"><img class="media-object" src="images/man4.jpg"
									alt=""></a>
							</div>
							<div class="media-body">
								<h4>Jhon Doe</h4>
								<h5>Founder and CEO</h5>
								<ul class="tag clearfix">
									<li class="btn"><a href="#">Web</a></li>
									<li class="btn"><a href="#">Ui</a></li>
									<li class="btn"><a href="#">Ux</a></li>
									<li class="btn"><a href="#">Photoshop</a></li>
								</ul>
								<ul class="social_icons">
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								</ul>
							</div>
						</div>
						<!--/.media -->
						<p>There are many variations of passages of Lorem Ipsum
							available, but the majority have suffered alteration in some
							form, by injected humour, or randomised words which don't look
							even slightly believable.</p>
					</div>
				</div>
			</div>
			<!--/.row-->
		</div>
		<!--section-->
	</div>
	<!--/.container--> </section>
	<!--/about-us-->

	<!-- bottom -->
	<jsp:include page="common/bottom.jsp" />
	<!--/bottom-->

	<!-- footer -->
	<jsp:include page="common/footer.jsp" />
	<!--/footer-->


	<script src="js/jquery.js"></script>
	<script type="text/javascript">
		$('.carousel').carousel()
	</script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/jquery.isotope.min.js"></script>
	<script src="js/main.js"></script>
	<script src="js/wow.min.js"></script>
</body>
</html>