<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<footer id="footer" class="midnight-blue">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				&copy; 2016 Pavel Šeda
			</div>
			<div class="col-sm-6">
				<ul class="pull-right">
					<li><a href="${pageContext.request.contextPath}/home">Home</a></li>
					<li><a href="${pageContext.request.contextPath}/about-us">O nás</a></li>
					<li><a href="${pageContext.request.contextPath}/contact-us">Kontaktuj nás</a></li>
				</ul>
			</div>
		</div>
	</div>
	</footer>
</body>
</html>