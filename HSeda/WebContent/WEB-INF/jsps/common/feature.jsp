<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<section id="feature" class="transparent-bg">
	<div class="container">
		<div class="center wow fadeInDown">
			<h2>Naše služby</h2>
			<p class="lead">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
				eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut
				enim ad minim veniam
			</p>
		</div>

		<div class="row">
			<div class="features">
				<div class="col-md-4 col-sm-6 wow fadeInDown"
					data-wow-duration="1000ms" data-wow-delay="600ms">
					<div class="feature-wrap">
						<i class="fa fa-bullhorn"></i>
						<h2>Právní poradenství</h2>
						<h3>
							Psaní smluv</br> Poradenství
						</h3>
					</div>
				</div>
				<!--/.col-md-4-->

				<div class="col-md-4 col-sm-6 wow fadeInDown"
					data-wow-duration="1000ms" data-wow-delay="600ms">
					<div class="feature-wrap">
						<i class="fa fa-comments"></i>
						<h2>Administrativní služby</h2>
						<h3>
							Zpracování dokumentů Plánování schůzek</br> Vyřizování komunikace
						</h3>
					</div>
				</div>
				<!--/.col-md-4-->

				<div class="col-md-4 col-sm-6 wow fadeInDown"
					data-wow-duration="1000ms" data-wow-delay="600ms">
					<div class="feature-wrap">
						<i class="fa fa-cloud-download"></i>
						<h2>Daně a účetnictví</h2>
						<h3>
							Vedení účetnictví</br> Poradenství</br> Daňová přiznání
						</h3>
					</div>
				</div>
				<!--/.col-md-4-->

				<div class="col-md-4 col-sm-6 wow fadeInDown"
					data-wow-duration="1000ms" data-wow-delay="600ms">
					<div class="feature-wrap">
						<i class="fa fa-leaf"></i>
						<h2>Jazykové služby</h2>
						<h3>
							Překlady</br>Psaní textů</br>Korektury textů</br>
						</h3>
					</div>
				</div>
				<!--/.col-md-4-->

				<div class="col-md-4 col-sm-6 wow fadeInDown"
					data-wow-duration="1000ms" data-wow-delay="600ms">
					<div class="feature-wrap">
						<i class="fa fa-cogs"></i>
						<h2>Internetové služby</h2>
						<h3>
							Správa sociálních sítí</br> Tvorba WWW stránek</br>Internetové rešerše
						</h3>
					</div>
				</div>
				<!--/.col-md-4-->

				<div class="col-md-4 col-sm-6 wow fadeInDown"
					data-wow-duration="1000ms" data-wow-delay="600ms">
					<div class="feature-wrap">
						<i class="fa fa-heart"></i>
						<h2>Další služby</h2>
						<h3>
							Rezervace restaurace</br>Tipy na výlety a dovolenou</br>Zajištění oslav
						</h3>
					</div>
				</div>
				<!--/.col-md-4-->
			</div>
			<!--/.services-->
		</div>
		<!--/.row-->
</section>