<head>
<!-- core CSS -->
<link href="${pageContext.request.contextPath}/static/css/bootstrap.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/static/css/font-awesome.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/static/css/prettyPhoto.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/static/css/animate.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/static/css/main.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/static/css/responsive.css" rel="stylesheet">

