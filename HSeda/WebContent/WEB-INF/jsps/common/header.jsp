<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Virtuální pomocník</title>
</head>
<body>
	<header id="header">
		<div class="top-bar">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-xs-4">
						<div class="top-number">
							<p>
								<i class="fa fa-phone-square"></i> +420 776 700 858
							</p>
						</div>
					</div>
					<div class="col-sm-6 col-xs-8">
						<div class="social">
							<ul class="social-share">
								<li><a href="https://www.facebook.com/pavel.seda.7"><i
										class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a
									href="https://www.linkedin.com/in/pavel-%C5%A1eda-b5b96184"><i
										class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-skype"></i></a></li>
							</ul>
							<div class="search">
								<form role="form">
									<input type="text" class="search-form" autocomplete="off"
										placeholder="Search"> <i class="fa fa-search"></i>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--/.container-->
		</div>
		<!--/.top-bar-->
		<nav class="navbar navbar-inverse" role="banner">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand"
						href="${pageContext.request.contextPath}/home"><img
						src="${pageContext.request.contextPath}/static/images/logo.png"
						alt="logo"></a>
				</div>

				<div class="collapse navbar-collapse navbar-right">
					<ul class="nav navbar-nav">
						<li><a href="${pageContext.request.contextPath}/home">Home</a></li>
						<li class="active"><a
							href="${pageContext.request.contextPath}/about-us">O nás</a></li>
						<li><a href="${pageContext.request.contextPath}/services">Služby</a></li>
						<li><a href="${pageContext.request.contextPath}/pricing">Ceník</a></li>
						<li><a href="${pageContext.request.contextPath}/portfolio">Portfolio</a></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown">Pages <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
								<li><a href="${pageContext.request.contextPath}/blog-item">Blog
										Single</a></li>
								<li><a href="${pageContext.request.contextPath}/error">Error</a></li>
								<li><a href="${pageContext.request.contextPath}/shortcodes">Shortcodes</a></li>
							</ul></li>
						<li><a href="${pageContext.request.contextPath}/blog">Blog</a></li>
						<li><a href="${pageContext.request.contextPath}/contact-us">Kontakt</a></li>
					</ul>
				</div>
			</div>
			<!--/.container-->
		</nav>
		<!--/nav-->
	</header>
</body>
</html>