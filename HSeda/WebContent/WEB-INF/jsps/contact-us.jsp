<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="common/head.jsp" />
</head>
<body>

	<!-- header -->
	<jsp:include page="common/header.jsp" />
	<!--/header-->

	<section id="contact-info">
	<div class="center">
		<h2>Kontakty na virtuálního pomocníka</h2>
		<p class="lead">Lorem ipsum dolor sit amet, consectetur
			adipisicing elit</p>
	</div>
	<div class="gmap-area">
		<div class="container">
			<div class="row">
				<div class="col-sm-5 text-center">
					<div class="gmap">
					    <!--  
						<iframe frameborder="0" scrolling="no" marginheight="0"
							marginwidth="0" src="https://www.google.cz/maps/place/Richtrova+386%2F3,+Brno-Kohoutovice,+623+00+Brno/@49.1951496,16.5390838,17z/data=!3m1!4b1!4m2!3m1!1s0x47129662a5094ca7:0xbf695497aacb0645"></iframe>
						-->
						<!--  -->
						<iframe frameborder="0" scrolling="no" marginheight="0"
							marginwidth="0"
							src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=JoomShaper,+Dhaka,+Dhaka+Division,+Bangladesh&amp;aq=0&amp;oq=joomshaper&amp;sll=37.0625,-95.677068&amp;sspn=42.766543,80.332031&amp;ie=UTF8&amp;hq=JoomShaper,&amp;hnear=Dhaka,+Dhaka+Division,+Bangladesh&amp;ll=23.73854,90.385504&amp;spn=0.001515,0.002452&amp;t=m&amp;z=14&amp;iwloc=A&amp;cid=1073661719450182870&amp;output=embed"></iframe>
						
					</div>
				</div>

				<div class="col-sm-7 map-content">
					<ul class="row">
						<li class="col-sm-6">
							<address>
								<h5>Hlavní kancelář</h5>
								<p>
									Richtrova 3 <br> Brno
								</p>
								<p>
									Phone:+420-776-700-858 <br>
									Email:Jindriska.Sedova@econ.muni.cz
								</p>
							</address>

							<address>
								<h5>Lokální kancelář</h5>
								<p>
									Rerychova 4 <br> Brno
								</p>
								<p>
									Phone:+420-776-700-858 <br>
									Email:pavelseda@email.cz
								</p>
							</address>
						</li>

						<li class="col-sm-6">
							<address>
								<h5>Lokální kancelář</h5>
								<p>
									Rerychova 4 <br> Brno
								</p>
								<p>
									Phone:+420-776-700-858 <br>
									Email:Z.Perutkova@seznam.cz
								</p>
							</address>

						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	</section>
	<!--/gmap_area -->

	<section id="contact-page">
	<div class="container">
		<div class="center">
			<h2>Zanechte mu e-mailovou zprávu</h2>
			<p class="lead">Sed do eiusmod tempor incididunt ut labore et
				dolore magna aliqua.</p>
		</div>
		<div class="row contact-wrap">
			<div class="status alert alert-success" style="display: none"></div>
			<form id="main-contact-form" class="contact-form" name="contact-form"
				method="post" action="sendemail.php">
				<div class="col-sm-5 col-sm-offset-1">
					<div class="form-group">
						<label>Name *</label> <input type="text" name="name"
							class="form-control" required="required">
					</div>
					<div class="form-group">
						<label>Email *</label> <input type="email" name="email"
							class="form-control" required="required">
					</div>
					<div class="form-group">
						<label>Phone</label> <input type="number" class="form-control">
					</div>
					<div class="form-group">
						<label>Company Name</label> <input type="text"
							class="form-control">
					</div>
				</div>
				<div class="col-sm-5">
					<div class="form-group">
						<label>Subject *</label> <input type="text" name="subject"
							class="form-control" required="required">
					</div>
					<div class="form-group">
						<label>Message *</label>
						<textarea name="message" id="message" required="required"
							class="form-control" rows="8"></textarea>
					</div>
					<div class="form-group">
						<button type="submit" name="submit" class="btn btn-primary btn-lg"
							required="required">Submit Message</button>
					</div>
				</div>
			</form>
		</div>
		<!--/.row-->
	</div>
	<!--/.container--> </section>
	<!--/#contact-page-->

	<!-- bottom -->
	<jsp:include page="common/bottom.jsp" />
	<!--/bottom-->

	<!-- footer -->
	<jsp:include page="common/footer.jsp" />
	<!--/footer-->

	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/jquery.isotope.min.js"></script>
	<script src="js/main.js"></script>
	<script src="js/wow.min.js"></script>
</body>
</html>