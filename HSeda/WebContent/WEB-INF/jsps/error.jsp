<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<head>
<jsp:include page="common/head.jsp" />
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
<link rel="shortcut icon" href="images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="images/ico/apple-touch-icon-57-precomposed.png">
</head>
<!--/head-->
<body>

	<!-- header -->
	<jsp:include page="common/header.jsp" />
	<!--/header-->

	<section id="error" class="container text-center">
		<h1>404, Page not found</h1>
		<p>The Page you are looking for doesn't exist or an other error
			occurred.</p>
		<a class="btn btn-primary"
			href="${pageContext.request.contextPath}/home">GO BACK TO THE
			HOMEPAGE</a>
	</section>
	<!--/#error-->

	<!-- bottom -->
	<jsp:include page="common/bottom.jsp" />
	<!--/bottom-->

	<!-- footer -->
	<jsp:include page="common/footer.jsp" />
	<!--/footer-->

	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/jquery.isotope.min.js"></script>
	<script src="js/main.js"></script>
	<script src="js/wow.min.js"></script>
</body>
</html>