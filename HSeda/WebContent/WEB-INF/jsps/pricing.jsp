<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="common/head.jsp" />
</head>
<body>

	<!-- header -->
	<jsp:include page="common/header.jsp" />
	<!--/header-->

	<section class="pricing-page">
		<div class="container">
			<div class="center">
				<h2>Ceník služeb</h2>
				<p class="lead">
					Příklady úkolů
				<ul>
					<li>Potřebuji denně kontrolovat schránku s poptávkami na
						školení, odepisovat potenciálním klientům a připravovat podklady
						pro nabídky.</li>
					<li>Prosím o zavolání následujícím 19 zákazníkům. Domluvte mi
						se všemi schůzky a naplánujte je tak, aby na sebe co nejlépe
						navazovaly. Zajistěte mi také ubytování a vyhledejte dopravní
						spojení.</li>
					<li>Tady je stránka naší firmy na Facebooku. Nemáme čas se jí
						věnovat, prosím zajistěte pravidelné přispívání alespoň 2x týdně a
						posílejte nám reporty o odezvě a počtu fanoušků.</li>
					<li>Pravidelně vám budeme posílat sadu dokumentů, které je
						třeba naskenovat, očíslovat a archivovat. Občas vám dodáme také
						tištěný dokument, který přepíšete a pošlete na následující
						e-mailovou adresu.</li>
				</ul>
				</p>
			</div>
			<div class="pricing-area text-center">
				<div class="row">
					<div class="col-sm-4 plan price-one wow fadeInDown">
						<ul>
							<li class="heading-one">
								<h1>Max</h1> <span>14900 Kč/30 dní</span>
							</li>
							<li>80 hodin/30 dní</li>
							<li>E-mailová komunikace</li>
							<li>Telefonická komunikace</li>
							<li>24/7 Podpora</li>
							<li class="plan-action"><a href="" class="btn btn-primary">Register</a>
							</li>
						</ul>
					</div>

					<div class="col-sm-4 plan price-two wow fadeInDown">
						<ul>
							<li class="heading-two">
								<h1>Standard</h1> <span>4 900 Kč/30 dní</span>
							</li>
							<li>20 hodin/30 dní</li>
							<li>E-mailová komunikace</li>
							<li>Telefonická komunikace</li>
							<li>24/7 Podpora</li>
							<li class="plan-action"><a href="" class="btn btn-primary">Register</a>
							</li>
						</ul>
					</div>

					<div class="col-sm-4 plan price-three wow fadeInDown">
						<img
							src="${pageContext.request.contextPath}/static/images/ribon_one.png">
						<ul>
							<li class="heading-three">
								<h1>Mini</h1> <span>2900 Kč/30 dní</span>
							</li>
							<li>10 hodin/30 dní</li>
							<li>E-mailová komunikace</li>
							<li>Telefonická komunikace</li>
							<li>24/7 Podpora</li>
							<li class="plan-action"><a href="" class="btn btn-primary">Register</a>
							</li>
						</ul>
					</div>

					<div class="col-sm-6 col-md-3 plan price-four wow fadeInDown">
						<ul>
							<li class="heading-four">
								<h1>Basic</h1> <span>$3/Month</span>
							</li>
							<li>5 Gb Disk Space</li>
							<li>1GB Dadicated Ram</li>
							<li>10 Addon Domain</li>
							<li>10 Email Account</li>
							<li>24/7 Support</li>
							<li class="plan-action"><a href="" class="btn btn-primary">Sign
									up</a></li>
						</ul>
					</div>

					<div class="col-sm-6 col-md-3 plan price-six wow fadeInDown">
						<img
							src="${pageContext.request.contextPath}/static/images/ribon_two.png">
						<ul>
							<li class="heading-six">
								<h1>Premium</h1> <span>$12/Month</span>
							</li>
							<li>5 Gb Disk Space</li>
							<li>1GB Dadicated Ram</li>
							<li>10 Addon Domain</li>
							<li>10 Email Account</li>
							<li>24/7 Support</li>
							<li class="plan-action"><a href="" class="btn btn-primary">Sign
									up</a></li>
						</ul>
					</div>

					<div class="col-sm-6 col-md-3 plan price-seven wow fadeInDown">
						<ul>
							<li class="heading-seven">
								<h1>Developer</h1> <span>$19/Month</span>
							</li>
							<li>5 Gb Disk Space</li>
							<li>1GB Dadicated Ram</li>
							<li>10 Addon Domain</li>
							<li>10 Email Account</li>
							<li>24/7 Support</li>
							<li class="plan-action"><a href="" class="btn btn-primary">Sign
									up</a></li>
						</ul>
					</div>
				</div>
			</div>
			<!--/pricing-area-->
		</div>
		<!--/container-->
	</section>
	<!--/pricing-page-->

	<!-- bottom -->
	<jsp:include page="common/bottom.jsp" />
	<!--/bottom-->

	<!-- footer -->
	<jsp:include page="common/footer.jsp" />
	<!--/footer-->

	<script src="js/jquery.js"></script>
	<script type="text/javascript">
		$('.carousel').carousel()
	</script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/jquery.isotope.min.js"></script>
	<script src="js/main.js"></script>
	<script src="js/wow.min.js"></script>
</body>
</html>
ml>
