<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="common/head.jsp" />
</head>
<body>

	<!-- header -->
	<jsp:include page="common/header.jsp" />
	<!--/header-->

	<jsp:include page="common/feature.jsp" />


	<div class="get-started center wow fadeInDown">
		<h2>Ready to get started</h2>
		<p class="lead">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
			eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>
			Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris
			nisi ut aliquip ex ea commodo
		</p>
		<div class="request">
			<h4>
				<a href="#">Request a free Quote</a>
			</h4>
		</div>
	</div>
	<!--/.get-started-->

	<div class="clients-area center wow fadeInDown">
		<h2>Co o nás říkají naši klienti</h2>
		<p class="lead">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
			eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut
			enim ad minim veniam
		</p>
	</div>

	<div class="row">
		<div class="col-md-4 wow fadeInDown">
			<div class="clients-comments text-center">
				<img src="${pageContext.request.contextPath}/static/images/client1.png" class="img-circle" alt="">
				<h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
					sed do eiusmod tempor incididunt</h3>
				<h4>
					<span>-John Doe /</span> Director of corlate.com
				</h4>
			</div>
		</div>
		<div class="col-md-4 wow fadeInDown">
			<div class="clients-comments text-center">
				<img src="${pageContext.request.contextPath}/static/images/client2.png" class="img-circle" alt="">
				<h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
					sed do eiusmod tempor incididunt</h3>
				<h4>
					<span>-John Doe /</span> Director of corlate.com
				</h4>
			</div>
		</div>
		<div class="col-md-4 wow fadeInDown">
			<div class="clients-comments text-center">
				<img src="${pageContext.request.contextPath}/static/images/client3.png" class="img-circle" alt="">
				<h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
					sed do eiusmod tempor incididunt</h3>
				<h4>
					<span>-John Doe /</span> Director of corlate.com
				</h4>
			</div>
		</div>
	</div>

	</div>
	<!--/.container-->
	</section>
	<!--/#feature-->


	<!-- bottom -->
	<jsp:include page="common/bottom.jsp" />
	<!--/bottom-->

	<!-- footer -->
	<jsp:include page="common/footer.jsp" />
	<!--/footer-->

	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/jquery.isotope.min.js"></script>
	<script src="js/main.js"></script>
	<script src="js/wow.min.js"></script>
</body>
</html>