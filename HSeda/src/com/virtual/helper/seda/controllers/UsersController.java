package com.virtual.helper.seda.controllers;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UsersController {

	@RequestMapping("/home")
	public String showHome(Model model) {
		return "home";
	}

	@RequestMapping("/error")
	public String showError(Model model) {
		return "error";
	}

	@RequestMapping("/about-us")
	public String showAboutUs(Model model) {
		return "about-us";
	}

	@RequestMapping("/blog")
	public String showBlog(Model model) {
		return "blog";
	}

	@RequestMapping("/blog-item")
	public String showBlogItem(Model model) {
		return "blog-item";
	}

	@RequestMapping("/contact-us")
	public String showContact(Model model) {
		return "contact-us";
	}

	@RequestMapping("/portfolio")
	public String showPortfolio(Model model) {
		return "portfolio";
	}

	@RequestMapping("/pricing")
	public String showPricing(Model model) {
		return "pricing";
	}

	@RequestMapping("/services")
	public String showServices(Model model) {
		return "services";
	}

	@RequestMapping("/shortcodes")
	public String showShortcodes(Model model) {
		return "shortcodes";
	}

	@RequestMapping(value = "/sendmessage", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Map<String, Object> sendMessage(Principal principal, @RequestBody Map<String, Object> data) {
		String text = (String) data.get("text");
		String name = (String) data.get("name");
		String email = (String) data.get("email");
		Integer target = (Integer) data.get("target");

		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setFrom("pavelseda@email.cz");
		mail.setTo(email);
		mail.setSubject("Re: " + name + ", your message");
		mail.setText(text);
		try {
			//mailSender.send(mail);
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Cant send messafge");
		}
		Map<String, Object> rval = new HashMap<String, Object>();
		rval.put("success", true);
		rval.put("target", target);

		return rval;

	}
}
